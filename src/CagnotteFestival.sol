pragma solidity >=0.4.22 <0.7.0;

import "github.com/OpenZeppelin/openzeppelin-solidity/contracts/math/SafeMath.sol";

contract Cogere {

    using SafeMath for uint;

    mapping(address => uint) organisateurs;
    address[] organisateursTab;
    address internal owner;

    constructor() internal {
        owner = msg.sender;
        organisateurs[msg.sender] = 100;
        organisateursTab.push(msg.sender);
    }

    function transferOrga(address orga, uint parts) public {
        require(organisateurs[msg.sender].sub(parts) < organisateurs[msg.sender], "Vous ne possèdez pas les parts nécessaires!");
        organisateurs[msg.sender] = organisateurs[msg.sender].sub(parts);
        organisateurs[orga] = parts;
        organisateursTab.push(orga);
    }

    function estOrga(address orga) public view returns (bool) {
        return organisateurs[orga] > 0;
    }
}

contract CagnotteFestival is Cogere{

    mapping(address => bool) festivaliers;
    mapping(string => uint) sponsors;
    uint private placeRestantes;
    uint private depensesTotales;
    uint private dateFestival;
    uint private dateLiquidation;
    uint private seuilDepensesJournalieres;
    uint private totalDepensesJournalieres;
    uint private datePremiereDepense;

    constructor(uint _places, uint _date, uint _depensesJournalieresMax) public {
        placeRestantes = _places;
        dateFestival = _date;
        dateLiquidation = dateFestival + 2 weeks;
        datePremiereDepense = 0;
        seuilDepensesJournalieres = _depensesJournalieresMax;
    }

    function acheterTicket() public payable {
        require(msg.value >= 500 finney, "Place à 0.5 ethers");
        require(placeRestantes > 0,"Plus de places !");
        festivaliers[msg.sender] = true;
        placeRestantes.sub(1);
    }

    function modifierSeuilDepenseJournaliere(uint _montant) public {
        require(estOrga(msg.sender));
        seuilDepensesJournalieres = _montant;
    }

    function depenseJournaliere(uint _montant) private returns(bool){
        if(datePremiereDepense == 0 || datePremiereDepense + 1 days < now) {
            datePremiereDepense = now;
            totalDepensesJournalieres = 0;
        }
        require(address(this).balance > _montant);
        require(totalDepensesJournalieres + _montant < seuilDepensesJournalieres);
        totalDepensesJournalieres += _montant;
        return true;
    }

    function payer(address payable _destinataire, uint _montant) public {
        require(depenseJournaliere(_montant));
        require(estOrga(msg.sender));
        require(_destinataire != address(0));
        require(_montant > 0);
        _destinataire.transfer(_montant);
    }

    function sponsoriser(string memory _nom) public payable {
        require(msg.value >= 30 ether,"La mise d'entrée est de 30 ether minimum");
        sponsors[_nom] = msg.value;
    }

    function liquidation() public payable {
        require(now >= dateLiquidation);
        require(estOrga(msg.sender));
        for(uint i = 0; i < organisateursTab.length; i++){
            address payable organisateur = address(uint160(organisateursTab[i]));
            uint montant = organisateurs[organisateur] / 100 * address(this).balance;
            organisateur.transfer(montant);
            organisateursTab[i] = address(0);
        }
        selfdestruct(address(uint160(owner)));
    }
}
